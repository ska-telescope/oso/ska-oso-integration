.. _deployment_to_kubernetes:

Deployment to Kubernetes
=========================

The ``ska-oso-integration`` Helm chart will deploy all of the OSO applications, the OSO TMC simulator and Taranta.

The chart will deploy ``ska-oso-integration`` twice: one for Mid and one for Low. There is an additional Ingress rule defined in this chart
to route requests to the ``<namespace>/low/oet/api`` path to the Low instance and a similar rule for Mid.

The pipeline for the ``ska-oso-integration`` project will provide a persistent deployment of the ``ska-oso-integration`` chart main branch to the STFC Kubernetes cluster.

A manual deployment from a feature branch can also be triggered - see :doc:`/internal/integrating_new_version`.

To deploy the Helm chart locally, run ``make k8s-install-chart``.

The ``ska-oso-integration`` chart use the defaults from the OSO application charts where possible. They can be overwritten in the values.yaml file. See the individual charts and application docs for info on configuring the applications.
