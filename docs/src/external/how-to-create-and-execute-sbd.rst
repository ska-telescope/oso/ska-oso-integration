.. _how-to-create-and-execute-sbd.rst:

*********************************************************
How-to: create and execute a Scheduling Block Definition
*********************************************************

A key piece of OSO functionality is for a member of SKAO staff to use the ODT to create and edit a SBDefinition, and
then this SBDefinition to the executed on the telescope. This guide will step through that, by doing the following:

#. Create a SBDefinition using the ODT and persist this in the ODA.
#. Create and run an Activity in the OET from this SBDefinition
#. Monitor the state of the execution
#. Examine the SBInstance and ExecutionBlock in the ODA

.. note::
   The examples in this guide are for a local deployment of the ``ska-oso-integration`` Helm chart. For the pipeline version,
   the same steps apply, with the ``KUBE_HOST`` set as the address of the SKAO STFC Kubernetes cluster. Due to the AAA on the cluster ingress,
   the commands should be executed from a terminal inside the cluster, using a tool like `coder <https://coder.k8s.stfc.skao.int>`_.

.. note::
   To run these steps against a real TMC system rather than the OSO TMC Simulator, the telescope devices need to be turned on after deploying them.
   In real operations, it is not expected that OSO will be responsible for this step.


1. Deploy the ``ska-oso-integration`` Helm chart
-------------------------------------------------

Follow the steps in :doc:`/deployment/deployment_to_kubernetes` to deploy the Chart locally. The services should then be available in the
``ska-oso-integration`` namespace with a ``KUBE_HOST`` that can be found by running ``minikube ip``.

This will deploy all the OSO applications plus the OSO TMC CentralNode/Subarray device simulators and Taranta. There will two instance of the OET (one for Mid and one for Low) that are configured to
send commands to the relevant TMC devices.

To monitor the execution using Taranta, at this point visit ``<KUBE_HOST>/ska-oso-integration/taranta/`` and start a dashboard with widgets monitoring the relevant devices. See Taranta docs for more details.


2. Create a SBDefinition using the ODT and persist this in the ODA.
--------------------------------------------------------------------

Visit the ODT UI at ``<KUBE_HOST>/ska-oso-integration/odt/``

and follow the workflow to create either a Mid or Low SBDefinition. Consult the `ODT UI User Guide <https://developer.skao.int/projects/ska-oso-odt-ui/en/latest/?badge=latest>`_ for more details.

Clicking 'Save to ODA' should persist the SBDefinition to the ODA and display the ``sbd_id`` in the UI. Copy this ``sbd_id`` for the next step.

3. Create and run an Activity in the OET from this SBDefinition
-----------------------------------------------------------------

In a terminal with the OET CLI installed and the OET_URL set to ``<KUBE_HOST>/ska-oso-integration/<SKA_TELESCOPE>/oet/api/v<OET_MAJOR_VERSION>`` (see the `OET docs <https://developer.skao.int/projects/ska-oso-oet/en/latest/index.html>`_ for more details) run the following command,
where ``SKA_TELESCOPE`` is either ``mid`` or ``low`` depending on what was created in the ODT UI.

.. code-block:: console

    oet activity run observe <sbd_id> --create_env=True


``observe`` is the name of the activity that the ODT UI will create by default.


4. Monitor the state of the execution
--------------------------------------

The OET command will keep an event stream open that should display events from the OET during the observation.

.. code-block:: console

      $ oet activity run observe sbd-t0001-20241115-00002 --create_env=True
      ID  Activity    SB ID                     Creation Time          Procedure ID  State
    ----  ----------  ------------------------  -------------------  --------------  -------
       1  observe     sbd-t0001-20241115-00002  2024-11-15 12:12:05               1  TODO
     For details on activity:- oet activity describe --aid=<ID>
     For details on script execution:- oet procedure describe --pid=<Procedure ID>

    Events
    ------

    - Subarray 1: resources allocated
    - Resources allocated using SB sbd-t0001-20241115-00002
    - Observation for SB sbd-t0001-20241115-00002 started
    - SB sbd-t0001-20241115-00002: configuring for scan 2
    - Subarray 1 configured
    - SB sbd-t0001-20241115-00002: scan 2 configuration complete
    - SB sbd-t0001-20241115-00002: scan 2 starting
    - Subarray 1: scan started
    - Subarray 1: scan complete
    - SB sbd-t0001-20241115-00002: scan 2 complete
    - Observation for SB sbd-t0001-20241115-00002 started
    - SB sbd-t0001-20241115-00002: configuring for scan 4
    - Subarray 1 configured
    - SB sbd-t0001-20241115-00002: scan 4 configuration complete
    - SB sbd-t0001-20241115-00002: scan 4 starting
    - Subarray 1: scan started
    - Subarray 1: scan complete
    - SB sbd-t0001-20241115-00002: scan 4 complete
    - Observation for SB sbd-t0001-20241115-00002 complete
    - Subarray 1: resources released
    ^C

The Taranta dashboards can be used to monitor the device states.

The ``oet activity describe ...`` and ``oet procedure describe ...`` commands can also be used to inspect the OET state.

5. Examine the SBInstance and ExecutionBlock in the ODA
--------------------------------------------------------

The ``oet activity describe --aid=<AID>`` command should show the ``sbi_id`` for the SBInstance that was created during this execution.

Using the ODA CLI with the ODA_URL ``<KUBE_HOST>/ska-oso-integration/oda/api/v<ODA_MAJOR_VERSION>`` (see the `OET docs <https://developer.skao.int/projects/ska-oso-oet/en/latest/index.html>`_ for more details), query the ODA for the ``sbi_id``.

.. code-block:: console

    $ oda sbis get <sbi_id>
    {
        "interface": "https://schema.skao.int/ska-oso-pdm-sbi/0.1",
        "sbi_id": "sbi-t0001-20241115-00003",
        "metadata": {
            "version": 1,
            "created_by": "DefaultUser",
            "created_on": "2024-11-15T12:12:05.418904Z",
            "last_modified_by": "DefaultUser",
            "last_modified_on": "2024-11-15T12:12:05.418904Z",
            "pdm_version": "15.4.0"
        },
        "telescope": "ska_low",
        "sbd_ref": "sbd-t0001-20241115-00002",
        "sbd_version": 1,
        "activities": [
            {
                "activity_ref": "observe",
                "executed_at": "2024-11-15T12:12:05.411677Z",
                "runtime_args": []
            }
        ]
    }


To find the ExecutionBlock, query by a recent date to get the ``eb_id`` then use this to fetch the entity.

.. code-block:: console

    $ oda ebs query --created_after=2024-11-01
    Query Parameters
    ---------------
    query_type = QueryType.CREATED_BETWEEN
    start = 2024-11-01 00:00:00

    Result
    -------
    [
        "eb-t0001-20241115-00004"
    ]

    $ oda ebs get eb-t0001-20241115-00004
    oda ebs get eb-t0001-20241115-00004
    {
        "eb_id": "eb-t0001-20241115-00004",
        "metadata": {
            "version": 1,
            "created_by": "DefaultUser",
            "created_on": "2024-11-15T12:12:14.942202Z",
            "last_modified_by": "DefaultUser",
            "last_modified_on": "2024-11-15T12:12:15.730220Z",
            "pdm_version": "15.4.0"
        },
        "telescope": "ska_low",
        "sbi_ref": "sbi-t0001-20241115-00003",
        "request_responses": [
            {
                "request": "ska_oso_scripting.functions.devicecontrol.assign_resources_from_cdm",
                "request_args": {
                    "args": [
    ...


