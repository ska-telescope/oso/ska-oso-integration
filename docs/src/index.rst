SKA OSO Integration
====================

``ska-oso-integration`` is a deployment of all the current OSO applications into a single environment (i.e. a namespace called ``ska-oso-integration``).

Observatory Science Operations (OSO) within the SKA is a suite of applications which handle the observation lifecycle from proposal design
to scheduling and execution. The applications consist of React user interfaces and Python REST API servers, all backend by the OSO Data Archive (ODA).

For more information on the design and architecture of OSO, see `Solution Intent <https://confluence.skatelescope.org/pages/viewpage.action?pageId=159387040>`_

While these tools can be deployed independently from their own pipelines, they are all interlinked and should communicate with a single instance of the ODA.
This repository provides a single place to integrate all the services and deploy them from the same pipeline, so they can be easily tested together
and used by stakeholders via consistent URLs.

For information on deploying and configuring the ``ska-oso-integration`` Helm chart in a given Kubernetes or local environment, see the 'Deploying and configuring' section.

For user information about this environment, see the 'User Guide'.

For developer information about this environment, see the 'Developer Information' section

For instructions on developing the application, see the `README <https://gitlab.com/ska-telescope/oso/ska-oso-oet/-/blob/main/README.md>`_



.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Table of Contents
  :hidden:

.. toctree::
    :maxdepth: 2
    :caption: General
    :hidden:

    general/overview.rst
    general/dependency_versions.rst


.. toctree::
    :maxdepth: 2
    :caption: Deploying and configuring
    :hidden:

    deployment/deployment_to_kubernetes.rst

.. toctree::
    :maxdepth: 2
    :caption: User Guide
    :hidden:

    external/how-to-create-and-execute-sbd.rst

.. toctree::
    :maxdepth: 2
    :caption: Developer Information
    :hidden:

    internal/integrating_new_version.rst

