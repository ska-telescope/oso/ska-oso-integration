.. _overview:

***************************
Overview
***************************

The purpose of this project is to provide a persistent OSO deployment for stakeholders and developers. This is achieved through a
single ``ska-oso-integration`` Helm chart which has all of the OSO application Helm charts as dependencies.

.. figure:: ../diagrams/export/oso-integration.png
   :align: center

The applications should be permanently available at URLs consistent with the OSO URL pattern (defined `here <https://confluence.skatelescope.org/display/SE/OSO+Deployments+and+URLs>`_).

The following table lists the release version of the application currently integrated (with links to their docs pages) and the associated URL.

.. csv-table:: OSO Applications in ska-oso-integration
   :file: project_info.csv
   :header-rows: 1
