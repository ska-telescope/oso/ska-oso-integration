.. _dependency_versions:

**********************
Dependency Versions
**********************

.. note::
   This page is less about the ``ska-oso-integration`` environment and more about the latest versions of the individual services (which may or may not be up to date in ``ska-oso-integration``). However, these docs are still the best place for this diagram.

OSO is a suite of applications and libraries based on a shared data architecture, with dependencies between them through both Python imports and RESTful API calls.

Ideally the latest released version of a dependency would always be used, in particular with all applications using the same ODA and PDM version.
However during this stage of development that might not be the case. The diagram below aims to give a view of the current state of the dependencies.

It is generated via a Python script (see the README for details on running this) and pulls the latest versions of the dependencies from the Gitlab repositories.

Key for the diagram:

#. The boxes represent OSO applications and libraries, with the version inside the box being the latest release from that repository.
#. The arrows from box X to box Y represents a dependency of X on Y.
#. The label on the arrow is the version of Y that X depends on.
#. If the major version of the dependency does not match the latest release, the arrow label is coloured red and an '**' appended.
#. The solid arrow represents a Python dependency. The dashed arrows represent a dependency on an API.


.. figure:: ../diagrams/export/dependency_versions_diagram.png
   :align: center


