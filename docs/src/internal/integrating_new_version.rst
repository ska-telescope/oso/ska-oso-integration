.. _integrating_new_version:

***************************
Integrating a new version
***************************

As OSO is made up of several services all tightly coupled with the ODA and the PDM, it is important to ensure
a change to a service works correctly with the other services. Part of the purpose of this project is to provide
a place to do that.

Each individual application repository will still run its own integration tests and deploy to a dev and integration environment
using its own pipeline. These integration tests should use the project umbrella chart, which should deploy all the lower level dependencies
required to run proper integration tests. For example, the ODT UI umbrella chart should deploy the ODT Services chart as a dependency,
so that the integration tests and integration environment actually test the integration between the deployable components.

Once the application is released, it should then be included in ska-oso-integration. In theory, if it has been properly tested against the lower level
dependencies before release, this should not cause any issues when being included. However, there will often be breaking changes to the lower level systems
(for example a change to the data model) which cascade upwards.

Deploying a dev branch of ``ska-oso-integration``
---------------------------------------------------

To test the deployment when integrating a new version, you can deploy from a feature branch using the 'deploy-dev' stage on the pipeline.

When deployed, the services should be accessible for 4 hours at the same URLs as the persistent environment, but with the namespace changed
from ``ska-oso-integration`` to ``dev-ska-oso-integration-<branch name>``.
