CAR_OCI_REGISTRY_HOST ?= artefact.skao.int
CAR_OCI_REGISTRY_USERNAME ?= ska-telescope
PROJECT_NAME = ska-oso-integration
RELEASE_NAME ?= test

# Set sphinx documentation build to fail on warnings (as it is configured
# in .readthedocs.yaml as well)
DOCS_SPHINXOPTS ?= -W --keep-going

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= $(PROJECT_NAME)

# KUBE_HOST defines the IP address of the Minikube ingress.
KUBE_HOST ?= http://`minikube ip`

K8S_CHART = ska-oso-integration

KUBE_NAMESPACE_URL ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)
PTT_SERVICES_URL ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)/ptt/api/v0
SLT_SERVICES_URL ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)/slt/api/v0
OSD_SERVICES_URL ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)/osd/api/v3

# For backend services like the ODT and PHT, the application running in a pod can communicate directly with the
# ODA kubernetes service. However, the PTT communicates directly with the ODA API from the browser, so needs to use the ingress url.
# TODO after BTM-2252 we should be able to rely on the default in the OET chart rather than having to set it here
ODA_SERVICE_URL ?= http://ska-db-oda-rest-$(RELEASE_NAME):5000/$(KUBE_NAMESPACE)/oda/api/v7
OSD_SERVICE_URL ?= http://ska-ost-osd-rest-$(RELEASE_NAME):5000/$(KUBE_NAMESPACE)/osd/api/v3
BACKEND_URL_OET ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)/oet/api/v6
# SENSCALC_API_URL is appended with the /api/... route inside the application
SENSCALC_API_URL ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)/senscalc
SENSCALC_UI_URL ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)/senscalc
ODA_URL ?= $(KUBE_HOST)/$(KUBE_NAMESPACE)/oda/api/v7

K8S_CHART_PARAMS += \
  --set ska-oso-odt-ui.backendURL=$(KUBE_NAMESPACE_URL) \
  --set ska-oso-oet-mid.rest.oda.url=$(ODA_SERVICE_URL) \
  --set ska-oso-oet-low.rest.oda.url=$(ODA_SERVICE_URL) \
  --set ska-oso-ptt.backendURL=$(PTT_SERVICES_URL) \
  --set ska-oso-slt-ui.backendURL=$(SLT_SERVICES_URL) \
  --set ska-oso-oet-ui.backendURL=$(PTT_SERVICES_URL) \
  --set ska-oso-oet-ui.backendURLOET=$(BACKEND_URL_OET) \
  --set ska-oso-oet-ui.backendURLODA=$(ODA_URL) \
  --set ska-ost-osd-ui.backendURL=$(OSD_SERVICES_URL) \
  --set ska-ost-senscalc-ui.ui.runtime.backendURL=$(SENSCALC_API_URL) \
  --set ska-ost-senscalc-ui.ui.ingress.contextUrl=$(SENSCALC_UI_URL) \
  --set ska-ost-osd-umbrella.ska-ost-osd.rest.osd.url=$(OSD_SERVICE_URL)

# Set cluster_domain to minikube default (cluster.local) in local development
# (CI_ENVIRONMENT_SLUG should only be defined when running on the CI/CD pipeline)
ifeq ($(CI_ENVIRONMENT_SLUG),)
ODA_LOCAL_PASSWORD=localpassword
# AWS connections will not work locally
AWS_KEY_PLACEHOLDER=""
K8S_CHART_PARAMS += \
  --set global.cluster_domain="cluster.local" \
  --set ska-db-oda-umbrella.ska-db-oda.secretProvider.enabled=false \
  --set ska-db-oda-umbrella.ska-db-oda.rest.postgres.password=$(ODA_LOCAL_PASSWORD) \
  --set ska-oso-oet-low.rest.oda.postgres.password=$(ODA_LOCAL_PASSWORD) \
  --set ska-oso-oet-mid.rest.oda.postgres.password=$(ODA_LOCAL_PASSWORD) \
  --set ska-oso-services.rest.oda.postgres.password=$(ODA_LOCAL_PASSWORD) \
  --set ska-oso-pht-services.secretProvider.enabled=false \
  --set ska-oso-slt-services.secretProvider.enabled=false \
  --set global.env.aws_slt_bucket_name=$(AWS_SLT_BUCKET_NAME) \
  --set global.env.aws_server_public_key=$(AWS_SERVER_PUBLIC_KEY) \
  --set global.env.aws_server_secret_key=$(AWS_SERVER_SECRET_KEY) \
  --set global.env.aws_server_bucket_region=$(AWS_SERVER_BUCKET_REGION)
endif

diagrams:  ## recreate diagrams whose source has been modified
	docker run --rm -v $(CURDIR):/data rlespinasse/drawio-export:v4.5.0 --format=png --on-changes --remove-page-suffix docs/src/diagrams

run-docs-scripts:
	python3 -m venv venv
	source venv/bin/activate
	pip3 install -r scripts/requirements.txt
	python3 scripts/dependency_versions_diagram.py
	python3 scripts/project_info_csv.py
	rm -rf venv

copy-docs-scripts-output:
	mv scripts/dependency_versions_diagram.png docs/src/diagrams/export/dependency_versions_diagram.png
	mv scripts/project_info.csv docs/src/general/project_info.csv

docs-update: run-docs-scripts copy-docs-scripts-output

# include makefile targets from the submodule
include .make/base.mk
include .make/k8s.mk
include .make/helm.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak