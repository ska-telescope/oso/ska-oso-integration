SKA OSO Integration
====================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-oso-integration/badge/?version=latest)](https://developer.skao.int/projects/ska-oso-integration/en/latest/?badge=latest)

This project contains a Helm umbrella chart which deploys all OSO applications to a single namespace. 
For more context, see ReadTheDocs.

# Configuring the applications

Each Helm dependency offers its own set of configurable values that can be overwritten in the ska-oso-integration chart.

For example, to store data in the ska-db-oda filesystem rather than deploying Postgres, add the following to values.yaml

```
  ska-db-oda-umbrella:
    ska-db-oda:
      backend:
        type: filsystem
        filesystem:
          use_pv: false
            pv_hostpath: /mnt/ska-db-oda-persistent-storage
            pv_mountpoint: /var/lib/oda
    postgresql:
       enabled: false
```

# Local deployment

To clone this repository, run

```
git clone --recurse-submodules git@gitlab.com:ska-telescope/oso/ska-oso-integration.git
```

To refresh the GitLab Submodule, execute below commands:

```
git submodule update --recursive --remote
git submodule update --init --recursive
```

To deploy the chart to Kubernetes

```
make k8s-install-chart
```

Then to uninstall it

```
make k8s-uninstall-chart
```


# Deployments from CICD

## Deploying to non-production environments

The pipeline deployments use the standard pipeline templates, with a few overrides.

From a feature branch, the ``deploy-dev-environment`` job can be manually triggered to start a temporary (4 hours) deployment to a namespace
called ``dev-ska-oso-integration-<BRANCH_NAME>``.

From the main branch, the ``deploy-integration`` job will automatically deploy the Helm chart to the ``ska-oso-integration`` namespace.

To find the URL for the environment, see the 'info' job of the CICD pipeline stage or for more details on the URL see ReadTheDocs.

# Documentation

Documentation can be found in the ``docs`` folder. To build docs, install the 
documentation specific requirements:

```
pip3 install -r docs/requirements.txt
```

and build the documentation (will be built in docs/build folder) with 

```
make docs-build html
```

## Dynamic diagrams and tables

The docs contain a diagram and table with the latest versions of the components. 
These are generated from scripts inside the scripts directory at the root of this repo that should 
be ran frequently to keep up to date. To do this, run

```
make docs-update
```

which will run the scripts and move the outputs to the relevant docs location.
