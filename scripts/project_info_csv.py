import csv
import os
from datetime import date

from projectdetails import *

SKA_OSO_INTEGRATION_URL = "https://k8s.stfc.skao.int/ska-oso-integration"
LAST_GENERATED_ON = f"Last generated on {date.isoformat(date.today())}"


def get_url(url_path: str | list[str]) -> str:
    # Returns the URL(s) of the application deployed in the ska-oso-integration environment.
    if isinstance(url_path, str):
        return SKA_OSO_INTEGRATION_URL + url_path
    elif isinstance(url_path, list):
        return "\n".join([SKA_OSO_INTEGRATION_URL + path for path in url_path])


def get_project_label_with_docs_link(project: ProjectDetails) -> str:
    # Returns the label with a link to the docs in rst format
    return f"`{project.label} <{project.docs_url}>`_"


def create_integration_version_csv():
    projects_in_repo = [
        ODA,
        OSO_SERVICES,
        ODT_UI,
        PHT_SERVICES,
        PHT_UI,
        PTT_SERVICES,
        PTT_UI,
        OET,
        SLT_UI,
        SLT_SERVICES,
        OSD,
        SENSCALC_UI,
        SENSCALC,
    ]

    headers = ["Application", "Version", "URL"]
    data = [
        [
            get_project_label_with_docs_link(project),
            project.semantic_version,
            get_url(project.url_path),
        ]
        for project in projects_in_repo
    ]
    created_on_row = ["", "", LAST_GENERATED_ON]

    with open(
        f"{os.path.dirname(__file__)}/project_info.csv", mode="w", newline=""
    ) as file:
        writer = csv.writer(file)
        writer.writerow(headers)
        writer.writerows(data)
        writer.writerow(created_on_row)


if __name__ == "__main__":
    create_integration_version_csv()
