from dataclasses import dataclass
from typing import Optional

from requests import Response, get

__all__ = [
    "ProjectDetails",
    "get_file_from_repo",
    "major_versions_are_same",
    "ODA",
    "OSO_SERVICES",
    "ODT_UI",
    "PHT_SERVICES",
    "PHT_UI",
    "PTT_SERVICES",
    "PTT_UI",
    "OET",
    "OET_CLIENT",
    "PDM",
    "SCRIPTING",
    "SENSCALC_UI",
    "SENSCALC",
    "CDM",
    "SLT_SERVICES",
    "SLT_UI",
    "OSD",
]


@dataclass
class ProjectDetails:

    label: str
    project: str
    url_path: Optional[str | list[str]] = None

    def __post_init__(self):
        self.semantic_version = get_project_version(self.project)
        self.docs_url = f"https://developer.skao.int/projects/{self.project.split('/')[1] if '/' in self.project else self.project}/en/{self.semantic_version}"
        self.major_version = major_version_from_semantic(self.semantic_version)
        if isinstance(self.url_path, str):
            self.url_path = self.url_path.replace(
                "v<MAJOR_VERSION>", f"v{self.major_version}"
            )
        elif isinstance(self.url_path, list):
            self.url_path = [
                url_path.replace("v<MAJOR_VERSION>", f"v{self.major_version}")
                for url_path in self.url_path
            ]


def get_file_from_repo(project: str, file: str) -> Response:
    response = get(f"https://gitlab.com/ska-telescope/{project}/-/raw/main/{file}")
    if 404 == response.status_code:
        response = get(
            f"https://gitlab.com/ska-telescope/{project}/-/raw/master/{file}"
        )
    return response


def get_project_version(project: str) -> str:
    response = get_file_from_repo(project, ".release")
    version_line = response.text.splitlines()[0]
    return version_line.split("=")[1]


def major_version_from_semantic(semantic_version: str) -> int:
    return int(semantic_version.split(".")[0])


def major_versions_are_same(semantic_version_a: str, semantic_version_b: str) -> bool:
    return major_version_from_semantic(
        semantic_version_a.strip().strip('"^')
    ) == major_version_from_semantic(semantic_version_b.strip().strip('"^'))


ODA = ProjectDetails("ODA", "db/ska-db-oda", "/oda/api/v<MAJOR_VERSION>/ui/")
OSO_SERVICES = ProjectDetails(
    "OSO Services", "oso/ska-oso-services", "/oso/api/v<MAJOR_VERSION>/ui/"
)
ODT_UI = ProjectDetails("ODT UI", "oso/ska-oso-odt-ui", "/odt/")
PHT_SERVICES = ProjectDetails(
    "PHT Services", "oso/ska-oso-pht-services", "/pht/api/v<MAJOR_VERSION>/ui/"
)
PHT_UI = ProjectDetails("PHT UI", "oso/ska-oso-pht-ui", "/pht/")
PTT_SERVICES = ProjectDetails(
    "PTT Services", "oso/ska-oso-ptt-services", "/ptt/api/v<MAJOR_VERSION>/ui/"
)
PTT_UI = ProjectDetails("PTT UI", "oso/ska-oso-ptt", "/ptt/")
OET = ProjectDetails(
    "OET", "oso/ska-oso-oet", ["/low/oet/api/v6/ui/", "/mid/oet/api/v6/ui/"]
)
OET_CLIENT = ProjectDetails("OET Client", "oso/ska-oso-oet-client")
PDM = ProjectDetails("PDM", "oso/ska-oso-pdm")
SCRIPTING = ProjectDetails("OSO Scripting", "oso/ska-oso-scripting")
SENSCALC = ProjectDetails(
    "Sens Calc API",
    "ost/ska-ost-senscalc",
    [
        "/senscalc/api/v<MAJOR_VERSION>/mid/ui/",
        "/senscalc/api/v<MAJOR_VERSION>/low/ui/",
    ],
)
SENSCALC_UI = ProjectDetails("Sens Calc UI", "ost/ska-ost-senscalc-ui", "/senscalc/")
CDM = ProjectDetails("CDM", "ska-tmc-cdm")
SLT_UI = ProjectDetails("SLT UI", "oso/ska-oso-slt-ui", "/slt/")
SLT_SERVICES = ProjectDetails(
    "SLT Services", "oso/ska-oso-slt-services", "/slt/api/v<MAJOR_VERSION>/ui/"
)
OSD = ProjectDetails("OSD", "ost/ska-ost-osd", "/osd/api/v<MAJOR_VERSION>/ui/")
