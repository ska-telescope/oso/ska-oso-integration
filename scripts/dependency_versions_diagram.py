import os
from datetime import date

from diagrams import Diagram, Edge, Node
from projectdetails import *

LAST_GENERATED_ON = f"Last generated on {date.isoformat(date.today())}"


def get_python_dependency_version(project: str, dependency: str) -> str:
    response = get_file_from_repo(project, "pyproject.toml")
    dependency = dependency.split("/")[1] if "/" in dependency else dependency
    dependency_line = next(
        obj
        for obj in response.text.splitlines()
        if f"{dependency}=" in obj.replace(" ", "")
    )
    version = dependency_line.split(" = ")[1].split('"')[1]
    return version


def get_api_dependency_version(project: str, dependency: str) -> str:
    # This is a bit hacky but assumes the project includes the API service in its umbrella chart
    response = get_file_from_repo(
        project, f"charts/{project.split('/')[1]}-umbrella/Chart.yaml"
    )
    dependency = dependency.split("/")[1]
    lines = response.text.splitlines()
    try:
        index = lines.index(f"- name: {dependency}")
    except ValueError:
        index = lines.index(f"- name: {dependency}-umbrella")
    return lines[index + 1].strip().strip("version: ")


def create_dependency_version_diagram():
    graph_attr = {"splines": "spline", "layout": "neato", "overlap": "scale"}
    with Diagram(
        LAST_GENERATED_ON,
        filename=f"{os.path.dirname(__file__)}/dependency_versions_diagram",
        direction="TB",
        graph_attr=graph_attr,
        node_attr={"labelloc": "c"},
    ):
        nodes = {
            PDM.project: Node(f"{PDM.label}\n{PDM.semantic_version}", pos="-3,0!"),
            ODA.project: Node(f"{ODA.label}\n{ODA.semantic_version}", pos="3,0!"),
            OET.project: Node(f"{OET.label}\n{OET.semantic_version}", pos="0,-3!"),
            SCRIPTING.project: Node(f"{SCRIPTING.label}\n{SCRIPTING.semantic_version}", pos="3,-3!"),
            CDM.project: Node(f"{CDM.label}\n{CDM.semantic_version}", pos="6,-3!"),
            OET_CLIENT.project: Node(f"{OET_CLIENT.label}\n{OET_CLIENT.semantic_version}", pos="0,-6!"),
            OSO_SERVICES.project: Node(f"{OSO_SERVICES.label}\n{OSO_SERVICES.semantic_version}", pos="0,3!"),
            PHT_SERVICES.project: Node(f"{PHT_SERVICES.label}\n{PHT_SERVICES.semantic_version}", pos="-3,3!"),
            PTT_SERVICES.project: Node(f"{PTT_SERVICES.label}\n{PTT_SERVICES.semantic_version}", pos="3,3!"),
            PTT_UI.project: Node(f"{PTT_UI.label}\n{PTT_UI.semantic_version}", pos="3,6!"),
            ODT_UI.project: Node(
                f"{ODT_UI.label}\n{ODT_UI.semantic_version}", pos="0,6!"),
            PHT_UI.project: Node(f"{PHT_UI.label}\n{PHT_UI.semantic_version}", pos="-3,6!"),
            SLT_SERVICES.project: Node(f"{SLT_SERVICES.label}\n{SLT_SERVICES.semantic_version}", pos="6,3!"),
            SLT_UI.project: Node(f"{SLT_UI.label}\n{SLT_UI.semantic_version}", pos="6,6!"),
            OSD.project: Node(f"{OSD.label}\n{OSD.semantic_version}", pos="9,0!"),
            SENSCALC.project: Node(f"{SENSCALC.label}\n{SENSCALC.semantic_version}", pos="-6,3!"),
            SENSCALC_UI.project: Node(f"{SENSCALC_UI.label}\n{SENSCALC_UI.semantic_version}", pos="-6,6!"),
        }

        def create_python_dependency_edge(project: ProjectDetails, depends_on: ProjectDetails):
            dependency_version = get_python_dependency_version(
                project.project, depends_on.project
            )
            if not major_versions_are_same(depends_on.semantic_version, dependency_version):
                colour = "red"
                label = dependency_version + " **"
            else:
                colour = "black"
                label = dependency_version

            nodes[project.project] >> Edge(label=label, fontcolor=colour) >> nodes[depends_on.project]

        def create_api_dependency_edge(
            project: ProjectDetails, depends_on: ProjectDetails
        ):
            dependency_version = get_api_dependency_version(
                project.project, depends_on.project
            )
            if not major_versions_are_same(depends_on.semantic_version, dependency_version):
                colour = "red"
                label = dependency_version + " **"
            else:
                colour = "black"
                label = dependency_version

            nodes[project.project] >> Edge(label=label, style="dashed", fontcolor=colour) >> nodes[depends_on.project]

        create_python_dependency_edge(ODA, PDM)

        create_python_dependency_edge(OSO_SERVICES, ODA)

        create_api_dependency_edge(ODT_UI, OSO_SERVICES)

        create_python_dependency_edge(PHT_SERVICES, PDM)
        create_python_dependency_edge(PHT_SERVICES, ODA)
        create_api_dependency_edge(PHT_UI, PHT_SERVICES)

        # OET has been updated to depend on the PDM only implicitly through the ODA
        # create_python_dependency_edge(OET, PDM)
        create_python_dependency_edge(OET, ODA)

        nodes[OET_CLIENT.project] >> Edge(style="dashed") >> nodes[OET.project]
        create_python_dependency_edge(OET, OET_CLIENT)

        create_python_dependency_edge(SCRIPTING, ODA)
        create_python_dependency_edge(SCRIPTING, OET)
        create_python_dependency_edge(SCRIPTING, PDM)
        create_python_dependency_edge(SCRIPTING, CDM)

        create_api_dependency_edge(SENSCALC_UI, SENSCALC)
        create_api_dependency_edge(PHT_UI, SENSCALC)

        create_api_dependency_edge(PTT_UI, PTT_SERVICES)
        create_python_dependency_edge(PTT_SERVICES, ODA)

        create_api_dependency_edge(SLT_UI, SLT_SERVICES)
        create_python_dependency_edge(SLT_SERVICES, ODA)


if __name__ == "__main__":
    create_dependency_version_diagram()
